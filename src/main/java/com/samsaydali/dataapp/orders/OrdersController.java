package com.samsaydali.dataapp.orders;

import com.samsaydali.dataapp.orders.models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {

    @Autowired
    private OrdersService service;

    @GetMapping
    public List<Order> findAll() {
        return service.findAll();
    }
}
