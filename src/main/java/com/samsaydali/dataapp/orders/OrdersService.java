package com.samsaydali.dataapp.orders;

import com.samsaydali.dataapp.orders.models.Order;
import com.samsaydali.dataapp.orders.repositories.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersService {

    @Autowired
    private OrdersRepository repository;

    public List<Order> findAll() {
        return repository.findAll();
    }


}
