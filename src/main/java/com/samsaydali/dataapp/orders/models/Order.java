package com.samsaydali.dataapp.orders.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
    public List<OrderItem> orderItems;

    public Integer itemsCount() {
        return orderItems.size();
    }
}
