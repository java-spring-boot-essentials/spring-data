package com.samsaydali.dataapp.orders.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.samsaydali.dataapp.products.models.Product;

import javax.persistence.*;

@Entity
@Table(name = "order_items")
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @ManyToOne
    @JsonIgnore
    public Order order;

    @ManyToOne()
    public Product product;
}
