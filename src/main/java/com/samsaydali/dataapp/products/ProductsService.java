package com.samsaydali.dataapp.products;

import com.samsaydali.dataapp.products.models.Product;
import com.samsaydali.dataapp.products.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductsService {

    @Autowired
    private ProductsRepository productsRepository;

    public List<Product> getProducts() {
        return this.productsRepository.findAll();
    }
}
