package com.samsaydali.dataapp.products.models;

import com.samsaydali.dataapp.subcategories.models.Subcategory;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    public List<ProductTranslation> translations;

    @ManyToOne
    @JoinColumn(name="subcategory_id")
    public Subcategory subcategory;

    public String getTitle() {
        return translations.get(0).title;
    }

}
