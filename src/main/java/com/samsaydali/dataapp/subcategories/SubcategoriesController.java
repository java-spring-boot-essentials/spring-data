package com.samsaydali.dataapp.subcategories;

import com.samsaydali.dataapp.subcategories.models.Subcategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.querydsl.core.types.Predicate;

@RestController
@RequestMapping("/api/subcategories")
public class SubcategoriesController {

    @Autowired
    private SubcategoriesService subcategoriesService;

    @GetMapping
    public Page<Subcategory> findAll(@QuerydslPredicate(root = Subcategory.class) Predicate predicate, Pageable pageable) {
        return subcategoriesService.findAll(predicate, pageable);
    }
}
