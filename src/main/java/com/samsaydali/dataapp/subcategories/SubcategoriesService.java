package com.samsaydali.dataapp.subcategories;

import com.querydsl.core.types.Predicate;
import com.samsaydali.dataapp.subcategories.models.Subcategory;
import com.samsaydali.dataapp.subcategories.repositories.SubcategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class SubcategoriesService {

    @Autowired
    private SubcategoriesRepository subcategoriesRepository;

    public Page<Subcategory> findAll(Predicate predicate, Pageable pageable) {
        return this.subcategoriesRepository.findAll(predicate, pageable);
    }
}
