package com.samsaydali.dataapp.subcategories.models;

import com.samsaydali.dataapp.zones.models.Zone;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "subcategories")
public class Subcategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @OneToMany(mappedBy = "subcategory", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    public List<SubcategoryTranslation> translations;

    @Column(name = "category_id")
    public Long categoryId;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "subcategory_zones",
            joinColumns = @JoinColumn(name = "subcategory_id"),
            inverseJoinColumns = @JoinColumn(name = "zone_id"))
    @Fetch(value = FetchMode.SUBSELECT)
    public Set<Zone> zones;

    public String getTitle() {
        return translations.get(0).title;
    }

}
