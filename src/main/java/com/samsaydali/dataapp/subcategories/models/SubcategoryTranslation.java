package com.samsaydali.dataapp.subcategories.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "subcategory_translations")
public class SubcategoryTranslation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column
    public String title;

    @ManyToOne
    @JoinColumn(name="subcategory_id")
    @JsonIgnore
    public Subcategory subcategory;
}
