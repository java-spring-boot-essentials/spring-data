package com.samsaydali.dataapp.subcategories.repositories;

import com.samsaydali.dataapp.subcategories.models.Subcategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.function.Predicate;

@Repository
public interface SubcategoriesRepository extends CrudRepository<Subcategory, Long>, QuerydslPredicateExecutor<Subcategory> {

}
