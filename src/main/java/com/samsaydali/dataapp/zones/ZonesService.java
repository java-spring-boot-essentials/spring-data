package com.samsaydali.dataapp.zones;

import com.samsaydali.dataapp.zones.models.Zone;
import com.samsaydali.dataapp.zones.repositories.ZonesQRepository;
import com.samsaydali.dataapp.zones.repositories.ZonesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZonesService {

    @Autowired
    private ZonesRepository zonesRepository;

    @Autowired
    private ZonesQRepository zonesQRepository;

    public List<Zone> findAll() {
        return this.zonesRepository.findAll();
    }

    public List<Zone> findAll(String name) {
        if (name == null) {
            return findAll();
        }
        return this.zonesQRepository.findByName(name);
    }
}
