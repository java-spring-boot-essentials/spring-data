package com.samsaydali.dataapp.zones.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.samsaydali.dataapp.subcategories.models.Subcategory;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "zones")
public class Zone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column
    public String name;

    @Column
    public Double fee;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "subcategory_zones",
            joinColumns = @JoinColumn(name = "zone_id"),
            inverseJoinColumns = @JoinColumn(name = "subcategory_id"))
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore()
    public Set<Subcategory> subcategories;

}
