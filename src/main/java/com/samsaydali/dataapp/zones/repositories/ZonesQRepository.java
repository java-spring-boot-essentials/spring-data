package com.samsaydali.dataapp.zones.repositories;


import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import com.samsaydali.dataapp.zones.models.QZone;
import com.samsaydali.dataapp.zones.models.Zone;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository
public class ZonesQRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Zone> findByName(String name) {
        final JPAQuery<Zone> query = new JPAQuery<>(entityManager);
        final QZone zone = QZone.zone;

        return query.from(zone)
                .where(zone.subcategories.isNotEmpty())
                .where(zone.name.contains(name))
                .orderBy(
                    zone.id.desc(), zone.name.desc()
                ).fetch();
    }
}
